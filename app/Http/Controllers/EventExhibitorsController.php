<?php

namespace App\Http\Controllers;

use App\Jobs\GenerateTicket;
use App\Jobs\SendAttendeeInvite;
use App\Jobs\SendAttendeeTicket;
use App\Jobs\SendMessageToAttendees;
use App\Models\Attendee;
use App\Models\Event;
use App\Models\EventStats;
use App\Models\Message;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Ticket;
use App\Models\Exhibitor;
use App\Models\Account;
use App\Models\User;
use Auth;
use Config;
use DB;
use Excel;
use Illuminate\Http\Request;
use Log;
use Mail;
use Omnipay\Omnipay;
use PDF;
use Validator;
use Illuminate\Support\Facades\Input;
use Hash;
use App\Attendize\Utils;

class EventExhibitorsController extends MyBaseController
{
    /**
     * Show the exhibitors list
     *
     * @param Request $request
     * @param $event_id
     * @return View
     */
    public function showExhibitors(Request $request, $event_id)
    {
        $allowed_sorts = ['first_name', 'email', 'company_name', 'phone', 'website'];
        $searchQuery = $request->get('?q');
        $sort_order = $request->get('sort_order') == 'asc' ? 'asc' : 'desc';
        $sort_by = (in_array($request->get('sort_by'), $allowed_sorts) ? $request->get('sort_by') : 'created_at');

        $event = Event::scope()->find($event_id);

        if ($searchQuery) {
            $exhibitors = $event->exhibitors()
                ->where(function ($query) use ($searchQuery) {
                    $query->orWhere('exhibitors.first_name', 'like', $searchQuery . '%')
                        ->orWhere('exhibitors.email', 'like', $searchQuery . '%')
                        ->orWhere('exhibitors.last_name', 'like', $searchQuery . '%');
                })
                ->orderBy($sort_by, $sort_order)
                ->select('exhibitors.*')
                ->paginate();
        } else {
            $exhibitors = $event->exhibitors()
                ->orderBy($sort_by, $sort_order)
                ->select('exhibitors.*')
                ->paginate();
        }
        $data = [
            'exhibitors'  => $exhibitors,
            'event'      => $event,
            'sort_by'    => $sort_by,
            'sort_order' => $sort_order,
            'q'          => $searchQuery ? $searchQuery : '',
        ];
        return view('ManageEvent.Exhibitors', $data);
    }

    /**
     * Show the 'Invite Exhibitor' modal
     *
     * @param Request $request
     * @param $event_id
     * @return string|View
     */
    public function showInviteExhibitor(Request $request, $event_id)
    {
    	
        $event = Event::scope()->find($event_id);

        /*
         * If there are no tickets then we can't create an exhibitor
         * @todo This is a bit hackish
         */
        return view('ManageEvent.Modals.InviteExhibitor', [
            'event'   => $event,
        ]);
    }

    /**
     * Invite an exhibitor
     *
     * @param Request $request
     * @param $event_id
     * @return mixed
     */
    public function postInviteExhibitor(Request $request, $event_id)
    {
        $is_attendize = Utils::isAttendize();
        $rules = [
            'first_name' => 'required',
            'email'      => 'email|required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status'   => 'error',
                'messages' => $validator->messages()->toArray(),
            ]);
        }
        $first_name = $request->get('first_name');
        $middle_name = $request->get('middle_name');
        $last_name = $request->get('last_name');
        $company = $request->get('company');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $website = $request->get('website');
        $password = mt_rand(1000, 9999);

        DB::beginTransaction();

        try {
            /*
             * Create the exhibitor account
             */
            $account_data['first_name'] = $first_name;
            $account_data['last_name'] = $last_name;
            $account_data['email'] = $email;
            $account_data['currency_id'] = config('attendize.default_currency');
            $account_data['timezone_id'] = config('attendize.default_timezone');
            $account = Account::create($account_data);
            /*
             * Create the exhibitor user
             */
            $user = new User();
            $user_data['first_name'] = $first_name;
            $user_data['last_name'] = $last_name;
            $user_data['email'] = $email;
            $user_data['password'] = Hash::make($password);
            $user_data['account_id'] = $account->id;
            $user_data['is_parent'] = 1;
            $user_data['is_registered'] = 1;
            $user = User::create($user_data);
            if ($is_attendize) {
                // TODO: Do this async?
                Mail::send('Emails.ConfirmEmail',
                    ['first_name' => $user->first_name, 'confirmation_code' => $user->confirmation_code, 'email'=>$user->email, 'password' => $password],
                    function ($message) use ($request) {
                        $message->to($request->get('email'), $request->get('first_name'))
                            ->subject('You are invited as Exhibitor in Attendize');
                    });
            }
            
            /*
             * Create the exhibitor
             */
            $ex = new Exhibitor();
            $ex->first_name = $first_name;
            $ex->middle_name = $middle_name;
            $ex->last_name = $last_name;
            $ex->company_name = $company;
            $ex->email = $email;
            $ex->phone = $phone;
            $ex->website = $website;
            $ex->event_id = $event_id;
            $ex->account_id = $account->id;
            $ex->organizer_account_id = Auth::user()->account_id;
            $ex->reference_index = 1;
            $ex->save();
            session()->flash('message', 'Exhibitor Successfully Invited');

            DB::commit();

            return response()->json([
                'status'      => 'success',
                'redirectUrl' => route('showEventExhibitors', [
                    'event_id' => $event_id,
                ]),
            ]);

        } catch (Exception $e) {

            Log::error($e);
            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'error'  => 'An error occurred while inviting this exhibitor. Please try again.'
            ]);
        }

    }

    /**
     * Show the 'Edit Exhibitor' modal
     *
     * @param Request $request
     * @param $event_id
     * @param $exhibitor_id
     * @return View
     */
    public function showEditExhibitor(Request $request, $event_id, $exhibitor_id)
    {
        $exhibitor = Exhibitor::findOrFail($exhibitor_id);

        $data = [
            'exhibitor' => $exhibitor,
            'event'    => $exhibitor->event,
        ];
        return view('ManageEvent.Modals.EditExhibitor', $data);
    }

    /**
     * Updates an exhibitor
     *
     * @param Request $request
     * @param $event_id
     * @param $exhibitor_id
     * @return mixed
     */
    public function postEditExhibitor(Request $request, $event_id, $exhibitor_id)
    {
        $rules = [
            'first_name' => 'required',
            'email'      => 'required|email',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status'   => 'error',
                'messages' => $validator->messages()->toArray(),
            ]);
        }

        $exhibitor = Exhibitor::findOrFail($exhibitor_id);
        $exhibitor->update($request->all());

        session()->flash('message', 'Successfully Updated Exhibitor');

        return response()->json([
            'status'      => 'success',
            'id'          => $exhibitor->id,
            'redirectUrl' => '',
        ]);
    }

    /**
     * Shows the 'Cancel Exhibitor' modal
     *
     * @param Request $request
     * @param $event_id
     * @param $exhibitor_id
     * @return View
     */
    public function showCancelExhibitor(Request $request, $event_id, $exhibitor_id)
    {
        $exhibitor = Exhibitor::findOrFail($exhibitor_id);

        $data = [
            'exhibitor' => $exhibitor,
            'event'    => $exhibitor->event,
        ];

        return view('ManageEvent.Modals.CancelExhibitor', $data);
    }

    /**
     * Cancels an exhibitor
     *
     * @param Request $request
     * @param $event_id
     * @param $exhibitor_id
     * @return mixed
     */
    public function postCancelExhibitor(Request $request, $event_id, $exhibitor_id)
    {
        $exhibitor = Exhibitor::findOrFail($exhibitor_id);
        $error_message = false; //Prevent "variable doesn't exist" error message

        if ($exhibitor->is_cancelled) {
            return response()->json([
                'status'  => 'success',
                'message' => 'Exhibitor Already Cancelled',
                'redirectUrl' => '',
            ]);
        }

        $data = [
            'exhibitor'   => $exhibitor,
            'email_logo' => $exhibitor->event->organiser->full_logo_path,
        ];

        if ($request->get('notify_exhibitor') == '1') {
            Mail::send('Emails.notifyCancelledExhibitor', $data, function ($message) use ($exhibitor) {
                $message->to($exhibitor->email, $exhibitor->full_name)
                    ->from(config('attendize.outgoing_email_noreply'), $exhibitor->event->organiser->name)
                    ->replyTo($exhibitor->event->organiser->email, $exhibitor->event->organiser->name)
                    ->subject('You\'re guess as Exhibitor has been Cancelled');
            });
        }

        if ($error_message) {
            return response()->json([
                'status'  => 'error',
                'message' => $error_message,
                'redirectUrl' => '',
            ]);
        }

        session()->flash('message', 'Successfully Cancelled Exhibitor');

        return response()->json([
            'status'      => 'success',
            'id'          => $exhibitor->id,
            'redirectUrl' => '',
        ]);
    }
}
