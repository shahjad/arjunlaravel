@extends('Emails.Layouts.Master')

@section('message_content')

<p>Hi there,</p>
<p>
    Your ticket for the event <b>{{{$exhibitor->event->title}}}</b> has been cancelled.
</p>

<p>
    You can contact <b>{{{$exhibitor->event->organiser->name}}}</b> directly at <a href='mailto:{{{$exhibitor->event->organiser->email}}}'>{{{$exhibitor->event->organiser->email}}}</a> or by replying to this email should you require any more information.
</p>
@stop

@section('footer')

@stop
