<?php

use Illuminate\Database\Migrations\Migration;

class CreateExhibitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exhibitors', function ($t) {
            $t->increments('id');
            $t->unsignedInteger('event_id')->index();

            $t->string('first_name');
            $t->string('middle_name');
            $t->string('last_name');
            $t->string('company_name');
            $t->string('email');
            $t->string('phone');
            $t->string('website');

            $t->string('reference', 20);
            $t->integer('private_reference_number')->index();

            $t->nullableTimestamps();
            $t->softDeletes();

            $t->boolean('is_cancelled')->default(false);
            $t->boolean('has_arrived')->default(false);
            $t->dateTime('arrival_time')->nullable();

            $t->unsignedInteger('account_id')->index();
            $t->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
            $t->unsignedInteger('organizer_account_id')->index();
            $t->foreign('organizer_account_id')->references('id')->on('accounts')->onDelete('cascade');

            $t->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::drop('exhibitors');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
