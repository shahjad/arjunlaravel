<?php


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Exhibitor;
use App\Models\Order;

class ExhibitorRefFix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exhibitors', function (Blueprint $table) {
            $table->integer('reference_index')->default(0);
        });

        $exhibitors = Exhibitor::all();

        foreach($exhibitors as $exhibitor) {
            $exhibitor->reference_index = explode('-', $exhibitor->reference)[1];
            $exhibitor->save();
        }

        Schema::table('exhibitors', function (Blueprint $table) {
            $table->dropColumn('reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exhibitors', function (Blueprint $table) {
            $table->string('reference');
            $table->dropColumn('reference_index');
        });
    }
}
